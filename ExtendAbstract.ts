

abstract class Computer{

    modello:string
    colore:string
    abstract getModello():string

}

class Accesori extends Computer{

    modello:string
    colore:string
    getModello(){
        return 'Lenovo'
    }

}

let accesor1 = new Accesori()

let accesor2 = accesor1.getModello()

console.log(accesor2)